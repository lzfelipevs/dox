<?php

ini_set('display_errors', 0 );
error_reporting(0);

$config = [
	'dominio' => 'https://vmarkethomol.com.br/app/vmarket-core/cms2/exibir/dox',
	'id_empresa' => 189,
	'id_site' => 191,
	'clearsale' => '',
	'google_tag' => '',
	'facebookapp' => '',
	'favicon' => 'images/nm.ico',
	'oggimg' => '',
	'css' => [
		'assets/css/font-awesome-cms2.min.css',
		'assets/css/cms2/dox/font-icons.min.css',
		'assets/css/animate.css',
		'assets/css/hamburgers.min.css',
		'assets/css/owl.carousel.min.css',
		'assets/css/swiper.min.css',
		'assets/css/cms2/dox/responsive.css',
		'assets/css/cms2/dox/theme-vendors.min.css',
		//$this->themeUrl.'bootstrap.min.css',
		$this->themeUrl.'css/fonts/stylesheet.css',
		//$this->themeUrl.'style.css'
	],
	'js' => [
		'assets/js/lib/jquery.mask.js',
		'assets/js/lib/popper.min.js',
		'assets/js/lib/bootstrap-material-design.min.js',
		// 'assets/js/lib/owl.carousel.min.js',
		// 'assets/js/lib/owl-custom.min.js',
		'assets/js/lib/swiper.min.js',
		'assets/js/cms2/dox/theme-vendors.min.js',
		//'assets/js/lib/SmoothScroll.js',
	],
	'components' => ['newsletter','statusDelivery'],
	'paginas' => [
		'index' => [
			'template' => 'index.html',
			'title' => 'DOX - Comida para seu Dog sem pegadinhas.',
			'keywords' => '',
			'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
			'components' => ['dox/destaques','banners', 'login'] 
        ],
    'cadastro' => [
		'template' => 'cadastro.html',
		'title' => 'Cadastro - DOX - Comida para seu Dog sem pegadinhas.',
		'keywords' => '',
		'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'components' => ['cadastroEmpresa']
		],
    'login' => [
		'template' => 'login.html',
		'title' => 'Login - DOX - Comida para seu Dog sem pegadinhas.',
		'keywords' => '',
		'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'components' => ['login', 'cadastroUsuario', 'resetSenha']
		],
	'perguntas-frequentes' => [
		'template' => 'perguntas-frequentes.html',
		'title' => 'Perguntas Frequentes -  Novamix Distribuidora',
		'keywords' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'components' => ['contato']
	],
	'politica-de-trocas' => [
		'template' => 'politica-de-trocas.html',
		'title' => 'Política de trocas -  Novamix Distribuidora',
		'keywords' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'components' => ['contato']
	],
		'minha-conta' => [
			'template' => 'minha-conta.html',
			'title' => 'Minha Conta - DOX - Comida para seu Dog sem pegadinhas.',
			'keywords' => '',
			'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
			'components' => ['cadastroUsuario', 'cadastroEndereco']
		],
		'meus-pedidos' => [
			'template' => 'meus-pedidos.html',
			'title' => 'Minha Conta -  DOX - Comida para seu Dog sem pegadinhas.',
			'keywords' => '',
			'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
			'components' => ['meusPedidos']
		],
		'pedido' => [
			'template' => 'pedido.html',
			'title' => 'Meu Pedido -  DOX - Comida para seu Dog sem pegadinhas.',
			'keywords' => '',
			'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
			'components' => ['pedido']
		],
		'checkout' => [
			'template' => 'checkout.html',
			'title' => 'Checkout - DOX - Comida para seu Dog sem pegadinhas.',
			'keywords' => '',
			'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
			'components' => ['checkout','login', 'cadastroUsuario', 'resetSenha']
		],	
    'contato' => [
		'template' => 'contato.html',
		'title' => 'Contato - DOX - Comida para seu Dog sem pegadinhas.',
		'keywords' => '',
		'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'components' => ['contato']
		],
    'trabalhe-conosco' => [
		'template' => 'trabalhe-conosco.html',
		'title' => 'Trabalhe conosco - DOX - Comida para seu Dog sem pegadinhas.',
		'keywords' => '',
		'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'components' => ['trabalheConosco']
		],		
	'sobre-nos' => [
		'template' => 'sobre-nos.html',
		'title' => 'Sobre nós - DOX - Comida para seu Dog sem pegadinhas.',
		'keywords' => '',
		'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'components' => ['produtoDetalhes']
		],
	'catalogo' => [
		'template' => 'catalogo.html',
		'title' => 'Catálogo - DOX - Comida para seu Dog sem pegadinhas.',
		'keywords' => '',
		'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'components' => ['produtoDetalhes']
		],
	'nossas-marcas' => [
		'template' => 'nossas-marcas.html',
		'title' => 'Nossas Marcas - DOX - Comida para seu Dog sem pegadinhas.',
		'keywords' => '',
		'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'components' => ['produtoDetalhes','banners']
		],
	'produto' => [
		'template' => 'produto.html',
		'title' => 'Produto - DOX - Comida para seu Dog sem pegadinhas.',
		'keywords' => '',
		'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'components' => ['dox/destaques','produtoDetalhes','buscaProdutosSidebar']
		],
	'produtos' => [
		'template' => 'produtos.html',
		'title' => 'Produtos - DOX - Comida para seu Dog sem pegadinhas.',
		'keywords' => '',
		'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'components' => ['listaProdutos','buscaProdutosSidebar','bannerTitulo','breadcrumbProdutos','listaMarcas']
		],
	'localizacao' => [
		'template' => 'localizacao.html',
		'title' => 'Localização - DOX - Comida para seu Dog sem pegadinhas.',
		'keywords' => '',
		'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'components' => []
	],
	'receitas' => [
		'template' => 'receitas.html',
		'title' => 'Receitas -  Novamix Distribuidora',
		'keywords' => '',
		'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'components' => ['listaReceitas']
	],	
	'receita' => [
		'template' => 'receita.html',
		'title' => 'Receita -  Novamix Distribuidora',
		'keywords' => '',
		'description' => 'DOX - Comida para seu Dog sem pegadinhas.',
		'components' => ['receitaDetalhes']
		],
	]
];