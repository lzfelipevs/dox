export default {
	init: function(vm){
		var isHome = false;
		$(document).click((e)=>{
			// Check if click was triggered on or within #menu_content

			if($(e.target).closest('.cart-shopping.carrinho').length>0){
				return true;
			}
			if($(e.target).closest('section.shopping-cart.open').length>0){
				return true;
			}
			if( $(e.target).closest(".shopping-cart").length > 0 && e.target.className!="btn-green") {

				return false;
			}else if(e.target.className=="click-cart-shopping"){
				return false;
			}else if(e.target.className=="btn-border-yellow"){
				return false;
			}
		   if($(".shopping-cart").hasClass("open")){
				vm.$data.carrinho.open = false
		   }
		});
		$(document).on('click', 'a.goTo', function (event) {
			event.preventDefault();
			$('html, body').animate({
				scrollTop: ($($.attr(this, 'href')).offset().top-60)
			}, 500);
		});

	}
}
